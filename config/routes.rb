Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    root "devise/registrations#new"
  end

  resources :hobbies, only: [:index, :new, :create]
  resources :hobby_users, only: [:create]
  resources :users, only: [:index]
  resources :nearest_users, only: [:index]
 end
