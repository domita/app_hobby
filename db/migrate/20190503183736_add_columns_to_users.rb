class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :ip_adress, :string
    add_column :users, :lat, :float
    add_column :users, :lon, :float
  end
end
