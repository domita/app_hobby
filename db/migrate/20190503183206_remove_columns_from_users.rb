class RemoveColumnsFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :latitude
    remove_column :users, :longitude
  end
end
