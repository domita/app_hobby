class AddNameAndUserIdToHobbies < ActiveRecord::Migration[5.2]
  def change
    add_column :hobbies, :name, :string
    add_column :hobbies, :user_id, :integer
  end
end
