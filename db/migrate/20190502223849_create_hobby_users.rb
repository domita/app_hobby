class CreateHobbyUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :hobby_users do |t|

      t.timestamps
      t.integer :hobby_id
      t.integer :user_id
    end
  end
end
