require 'rails_helper'

describe User do
   describe "#check_nearest_users" do

     before do
      @user = User.create(email: "test@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[0])
      User.create(email: "test1@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[0])
      User.create(email: "test2@example.com", password: "example", password_confirmation: "example", ip_adress: australian_ips[0])
      User.create(email: "test3@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[1])
      User.create(email: "test4@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[2])
      User.create(email: "test5@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[3])
      User.create(email: "test6@example.com", password: "example", password_confirmation: "example", ip_adress: australian_ips[1])
      User.create(email: "test7@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[4])
     end

     let(:australian_ips) do
      ["122.201.117.255", "122.201.117.0"]
     end

     let(:poland_ips) do
      ["178.218.225.3", "185.7.169.78", "193.42.154.252", "78.133.206.129", "82.177.176.1"]
     end

     subject do
      @user.check_nearest_users
     end

     it "returns 5 elements array" do
      expect(subject.length).to eq(5)
     end

     it "returns only polish ips" do
      expect(subject.select{ |array| array[0].ip_adress.in?(australian_ips) }.present?).to eq(false)
     end

   end
 end


