require 'rails_helper'

feature "Filling registration form", type: :feature do

  background do
    visit '/users/sign_up'
  end

  feature "Successful path" do
    scenario "Redirects to hobbies list" do
      within("#new_user") do
        fill_in 'user_email', with: 'test@example.com'
        fill_in 'user_password', with: 'example'
        fill_in 'user_password_confirmation', with: 'example'
      end
      click_button "Sign up"
      expect(page).to have_content 'Hobbies'
    end

    scenario "Creates users" do
      within("#new_user") do
        fill_in 'user_email', with: 'test@example.com'
        fill_in 'user_password', with: 'example'
        fill_in 'user_password_confirmation', with: 'example'
      end
      expect { click_button "Sign up" }.to change { User.count }.by(1)
      user = User.last
      expect(user.email).to eq('test@example.com')
     end
  end

  feature "Validation errors" do
    scenario "Missing email" do
      within("#new_user") do
        fill_in 'user_email', with: ''
        fill_in 'user_password', with: 'example'
        fill_in 'user_password_confirmation', with: 'example'
      end
      click_button "Sign up"
      expect(page).to_not have_content 'Hobbies'
      expect(page).to have_content 'Password confirmation'
    end
  end

end

