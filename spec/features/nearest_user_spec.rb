require 'rails_helper'

feature "Showing nearest users", type: :feature do

  given(:user) do
    User.create(email: "test@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[0])
  end

  given(:australian_ips) do
    ["122.201.117.255", "122.201.117.0"]
  end

  given(:poland_ips) do
    ["178.218.225.3", "185.7.169.78", "193.42.154.252", "78.133.206.129", "82.177.176.1"]
  end

  background do
    User.create(email: "test1@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[0])
    User.create(email: "test2@example.com", password: "example", password_confirmation: "example", ip_adress: australian_ips[0])
    User.create(email: "test3@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[1])
    User.create(email: "test4@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[2])
    User.create(email: "test5@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[3])
    User.create(email: "test6@example.com", password: "example", password_confirmation: "example", ip_adress: australian_ips[1])
    User.create(email: "test7@example.com", password: "example", password_confirmation: "example", ip_adress: poland_ips[4])
    login_as(user, :scope => :user, :run_callbacks => false)
    visit '/nearest_users'
   end

  feature "Successful path" do
    scenario "Showing only nearest users" do
      expect(page).to have_content 'test1@example.com'
      expect(page).to have_content 'test3@example.com'
      expect(page).to have_content 'test4@example.com'
      expect(page).to have_content 'test5@example.com'
      expect(page).to have_content 'test7@example.com'
    end
    scenario "Not showing farthest users" do
      expect(page).to_not have_content 'test2@example.com'
      expect(page).to_not have_content 'test6@example.com'
    end
  end

end
