require 'rails_helper'

feature "Filling hobbies form", type: :feature do

  given(:user) do
    User.create(email: "test@example.com", password: "example", password_confirmation: "example")
  end

  background do
    login_as(user, :scope => :user, :run_callbacks => false)
    visit '/hobbies/new'
  end

  feature "Successful path" do
    scenario "Redirects to hobbies list" do
      within("#new_hobby") do
        fill_in 'hobby_name', with: 'reading'
      end
      click_button "Save"
      expect(page).to have_content 'Hobbies'
    end

    scenario "Creates hobbies" do
      within("#new_hobby") do
      fill_in 'hobby_name', with: 'reading'
      end
      expect { click_button "Save" }.to change { Hobby.count }.by(1)
      hobby = Hobby.last
      expect(hobby.name).to eq('reading')
    end
  end

  feature "Validation errors" do
    scenario "Missing hobby" do
      within("#new_hobby") do
        fill_in 'hobby_name', with: ''
      end
      click_button "Save"
      expect(page).to_not have_content 'Hobbies'
      expect(page).to have_content 'New hobby'
    end
  end

end
