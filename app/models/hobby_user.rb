class HobbyUser < ApplicationRecord
  belongs_to :hobby
  belongs_to :user
  validates :hobby_id, presence: true
  validates :user_id, presence: true
  validates :hobby_id, uniqueness: {scope: :user_id}
end
