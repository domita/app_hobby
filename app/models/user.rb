class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :hobby_users
  has_many :hobbies, through: :hobby_users

  geocoded_by :ip_adress, :latitude => :lat, :longitude => :lon
  after_validation :geocode

  def check_nearest_users
    array = []
    User.all.each do |user|
      if (user != self)
        array.push([user, self.distance_to(user, :km)])
      end
    end
    array.sort_by!{ |x,y| y }
    array[0..4]
  end

end
