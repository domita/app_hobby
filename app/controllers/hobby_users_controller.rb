class HobbyUsersController < ApplicationController

  def create
    HobbyUser.create(user_id: current_user.id, hobby_id: params[:hobby_id])
    redirect_to hobbies_path
  end

end

