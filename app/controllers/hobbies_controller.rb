class HobbiesController < ApplicationController

  def index
    @hobbies = Hobby.all
    @hobby_user = HobbyUser.new
  end

  def new
    @hobby = Hobby.new
  end

  def create
    combined_create_params = create_params
    combined_create_params[:user_id] = current_user.id
    @hobby = Hobby.create(combined_create_params)

    if @hobby.save
       redirect_to hobbies_path
    else
      render :new
    end
  end

  private

  def create_params
    params.require(:hobby).permit(:name)
  end
end
