class ApplicationController < ActionController::Base
   before_action :authenticate_user!
   before_action :set_ip_address

   def after_sign_in_path_for(resource)
    hobbies_path
   end

   def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
   end

   def set_ip_address
     if current_user
      current_user.update(ip_adress: request.ip)
     end
   end

end
