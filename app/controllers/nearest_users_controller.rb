class NearestUsersController < ApplicationController
  def index
    @nearest_users = current_user.check_nearest_users
  end
end
